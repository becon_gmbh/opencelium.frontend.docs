﻿##################
Welcome to OpenCelium documentation
##################

------------

This documentation is organized into a couple of sections:

.. toctree::
   :caption: Getting Started
   :maxdepth: 2

   gettinginvolved/introduction
   gettinginvolved/requirements
   gettinginvolved/installation

.. toctree::
   :caption: Usage
   :maxdepth: 2

   usage/login
   usage/application_management
   usage/users
   usage/groups
   usage/connectors
   usage/connections
   usage/schedules
   usage/apps


##################
License
##################

`becon`_ © 2013-2019 becon GmbH

.. _becon: LICENSE.html
      
